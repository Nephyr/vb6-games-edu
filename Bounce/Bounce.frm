VERSION 5.00
Begin VB.Form Dialog 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Nuova Simulazione"
   ClientHeight    =   3015
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   6030
   Icon            =   "Bounce.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3015
   ScaleWidth      =   6030
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   1
      Left            =   4680
      Top             =   720
   End
   Begin VB.TextBox new_sim 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   240
      TabIndex        =   5
      Top             =   1920
      Width           =   4215
   End
   Begin VB.TextBox new_sim 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   240
      TabIndex        =   4
      Top             =   1320
      Width           =   2295
   End
   Begin VB.Frame Frame1 
      Caption         =   "Imposta "
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2775
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   4455
      Begin VB.TextBox txt_x 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3240
         TabIndex        =   12
         Top             =   1200
         Width           =   1095
      End
      Begin VB.TextBox txt_y 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3240
         TabIndex        =   10
         Top             =   600
         Width           =   1095
      End
      Begin VB.CheckBox Graphic 
         Caption         =   " Simula utilizzando una linea contigua..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   2280
         Width           =   4215
      End
      Begin VB.TextBox new_sim 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   120
         TabIndex        =   2
         Top             =   600
         Width           =   2295
      End
      Begin VB.Label Label5 
         Alignment       =   2  'Center
         Caption         =   "di Max"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2520
         TabIndex        =   13
         Top             =   1230
         Width           =   615
      End
      Begin VB.Label Label4 
         Alignment       =   2  'Center
         Caption         =   "di Max"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2520
         TabIndex        =   11
         Top             =   630
         Width           =   615
      End
      Begin VB.Label Label3 
         Caption         =   "Velocit� Simulazione da 1 a 1000 ml sec."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   1560
         Width           =   4215
      End
      Begin VB.Label Label2 
         Caption         =   "Spostamento del corpo?"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   960
         Width           =   3135
      End
      Begin VB.Label Label1 
         Caption         =   " Altezza di lancio corpo?"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   3
         Top             =   360
         Width           =   3135
      End
   End
   Begin VB.CommandButton OKButton 
      Caption         =   "Simula"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4680
      TabIndex        =   0
      Top             =   240
      Width           =   1215
   End
   Begin VB.Label Divisione 
      Caption         =   "2"
      Height          =   255
      Left            =   4680
      TabIndex        =   8
      Top             =   1320
      Visible         =   0   'False
      Width           =   1215
   End
End
Attribute VB_Name = "Dialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private Sub OKButton_Click()

  If (Val(new_sim(0).Text) <> 0) And (Val(new_sim(1).Text) <> 0) And (Val(new_sim(2).Text) <> 0) And ((Val(new_sim(2).Text) > 0) And (Val(new_sim(2).Text) < 1001)) Then
  
   If (Val(new_sim(0).Text) <= Val(txt_y.Text)) And (Val(new_sim(1).Text) <= Val(txt_x.Text)) Then
   
     Rem Nascondo il Dialogo
     Dialog.Hide
     
     Rem Avvio il timer
     Timer1.Enabled = True
     
     Rem Imposto ci� che devo impostare
     Timer1.Interval = new_sim(2).Text
     
   Else
   
     MsgBox ("    Inserire valori che rimangano entro l'applicazione!!    ")
   
   End If
   
  Else
  
     MsgBox ("    Inserire dei valori numerici!!    ")
  
  End If

End Sub

Private Sub Timer1_Timer()

  Rem Dichiarazioni
  Dim A, B, X, Y, Dimezza As Single
  If Graphic = Checked Then
  
      Rem Disegno ed Imposto il Picture_Box
      Form1.Pict.ScaleHeight = -(Form1.Pict.Height)
      Form1.Pict.ScaleWidth = Form1.Pict.Width
      Form1.Pict.ScaleMode = 0
      Form1.Pict.ScaleTop = Form1.Pict.Height - 300
      Form1.Pict.DrawWidth = 28
      Form1.Pict.Line (0, -230)-(Form1.Pict.Width, -230), &HC0C0C0
      Form1.Pict.DrawWidth = 1
      Form1.Pict.Line (0, -10)-(Form1.Pict.Width, -10), vbBlack
      
      Rem Ricavo informazioni di calcolo
      A = Fix(Abs(new_sim(0).Text))
      B = Fix(Abs(new_sim(1).Text))
      X = Fix(Abs(Form1.x_spost(1).Text))
      Y = Fix(Abs(Form1.y_spost(0).Text))
      Dimezza = Divisione.Caption
     
      If Y <= 0 Then
      
         Form1.Pict.PSet (X, -Abs(Fix(A * Cos((6.28 / B) * X) / Dimezza))), vbBlue
         Form1.x_spost(1).Text = X + 1
         Form1.y_spost(0).Text = -Abs(Fix(A * Cos((6.28 / B) * X) / Dimezza))
         
      Else
      
         Form1.Pict.PSet (X, Abs(Fix(A * Cos((6.28 / B) * X) / Dimezza))), vbBlue
         Form1.x_spost(1).Text = X + 1
         Form1.y_spost(0).Text = Abs(Fix(A * Cos((6.28 / B) * X) / Dimezza))
      
      End If
      
      If X = Form1.Pict.Width Then
   
         Timer1.Enabled = False
   
      End If
      
      If Y = 0 Then
   
         Divisione.Caption = Divisione.Caption + 1
   
      End If
      
  Else
        
      Rem Disegno ed Imposto il Picture_Box
      Form1.Pict.Cls
      Form1.Pict.ScaleHeight = -(Form1.Pict.Height)
      Form1.Pict.ScaleWidth = Form1.Pict.Width
      Form1.Pict.ScaleMode = 0
      Form1.Pict.ScaleTop = Form1.Pict.Height - 300
      Form1.Pict.DrawWidth = 10
      Form1.Pict.Line (0, -230)-(Form1.Pict.Width, -230), &HC0C0C0
      Form1.Pict.DrawWidth = 1
      Form1.Pict.Line (0, -140)-(Form1.Pict.Width, -140), vbBlack
      
      Rem Ricavo informazioni di calcolo
      A = Fix(Abs(new_sim(0).Text))
      B = Fix(Abs(new_sim(1).Text))
      X = Fix(Abs(Form1.x_spost(1).Text))
      Y = Fix(Abs(Form1.y_spost(0).Text))
      Dimezza = Divisione.Caption
     
      If Y <= 0 Then
      
         Form1.Pict.Circle (X, -Abs(Fix(A * Cos((6.28 / B) * X) / Dimezza))), 150, vbBlue
         Form1.x_spost(1).Text = X + 1
         Form1.y_spost(0).Text = -Abs(Fix(A * Cos((6.28 / B) * X) / Dimezza))
         
      Else
      
         Form1.Pict.Circle (X, Abs(Fix(A * Cos((6.28 / B) * X) / Dimezza))), 150, vbBlue
         Form1.x_spost(1).Text = X + 1
         Form1.y_spost(0).Text = Abs(Fix(A * Cos((6.28 / B) * X) / Dimezza))
      
      End If
      
      If X = Form1.Pict.Width Then
   
         Timer1.Enabled = False
   
      End If
      
      If Y = 0 Then
   
         Divisione.Caption = Divisione.Caption + 1
   
      End If
      
  End If

End Sub

Private Sub Form_Unload(Cancel As Integer)
    Form1.del.Enabled = True
    Form1.sim.Caption = "Nuova Simulazione"
    Form1.Pict.Cls
    Form1.x_spost(1).Text = 1
    Form1.y_spost(0).Text = 1
End Sub
