VERSION 5.00
Begin VB.Form Form1 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   Caption         =   "Bounce Simulation"
   ClientHeight    =   8295
   ClientLeft      =   165
   ClientTop       =   525
   ClientWidth     =   12735
   Icon            =   "Bounce1.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8295
   ScaleWidth      =   12735
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox y_spost 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   11040
      TabIndex        =   2
      Text            =   "1"
      Top             =   0
      Width           =   1575
   End
   Begin VB.TextBox x_spost 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   9360
      TabIndex        =   1
      Text            =   "1"
      Top             =   0
      Width           =   1575
   End
   Begin VB.PictureBox Pict 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   8295
      Left            =   0
      Picture         =   "Bounce1.frx":13772
      ScaleHeight     =   8265
      ScaleWidth      =   12705
      TabIndex        =   0
      Top             =   0
      Width           =   12735
   End
   Begin VB.Menu del 
      Caption         =   "Cancella Schermo"
   End
   Begin VB.Menu sim 
      Caption         =   "Nuova Simulazione"
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub del_Click()

   Pict.Cls
   Dialog.Timer1.Enabled = False
   x_spost(1).Text = 1
   y_spost(0).Text = 1

End Sub

Private Sub Form_Resize()

   Rem Ridimensiona gli oggetti del form
   w = Form1.Width
   h = Form1.Height
   
   Pict.Width = w - 120
   Pict.Height = h - 780
   
   y_spost(0).Left = w - 2390
   x_spost(1).Left = w - 4100
   
   Dialog.txt_y.Text = h
   Dialog.txt_x.Text = Fix(w / 3)
 
End Sub

Private Sub sim_Click()

   If sim.Caption = "Nuova Simulazione" Then
     
     Dialog.Show
     sim.Caption = "Ferma Simulazione"
     del.Enabled = False
   
   Else
   
     Dialog.Timer1.Enabled = False
     sim.Caption = "Nuova Simulazione"
     del.Enabled = True
     Pict.Cls
     x_spost(1).Text = 1
     y_spost(0).Text = 1
   
   End If

End Sub

Private Sub Form_Unload(Cancel As Integer)
    Dim Risp As Integer
    Risp = MsgBox("Sei sicuro di voler terminare la simulazione?", vbYesNo)
    If Risp = vbNo Then
        Cancel = True
    Else
        End
    End If
End Sub
