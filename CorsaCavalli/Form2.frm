VERSION 5.00
Begin VB.Form Form2 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   0  'None
   Caption         =   "Risultati Vincita"
   ClientHeight    =   2895
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   4455
   Icon            =   "Form2.frx":0000
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   2895
   ScaleWidth      =   4455
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox res 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   2160
      TabIndex        =   4
      Text            =   "0"
      Top             =   1800
      Width           =   2055
   End
   Begin VB.TextBox res 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   2160
      TabIndex        =   3
      Text            =   "0"
      Top             =   1080
      Width           =   2055
   End
   Begin VB.TextBox res 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   2160
      TabIndex        =   2
      Text            =   "0"
      Top             =   720
      Width           =   2055
   End
   Begin VB.CommandButton close 
      Caption         =   "esci dal gioco"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   2400
      Width           =   2055
   End
   Begin VB.CommandButton new 
      Caption         =   "nuova partita"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2280
      TabIndex        =   0
      Top             =   2400
      Width           =   2055
   End
   Begin VB.Line Line8 
      X1              =   2220
      X2              =   2220
      Y1              =   100
      Y2              =   380
   End
   Begin VB.Label lbl_cav 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   2400
      TabIndex        =   9
      Top             =   120
      Width           =   1935
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "Totale Euro utente"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   8
      Top             =   1820
      Width           =   1935
   End
   Begin VB.Label operazione 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "..."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   7
      Top             =   1100
      Width           =   1935
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "Euro in possesso"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   6
      Top             =   735
      Width           =   1935
   End
   Begin VB.Line Line7 
      X1              =   120
      X2              =   4320
      Y1              =   1560
      Y2              =   1560
   End
   Begin VB.Line Line6 
      X1              =   120
      X2              =   4320
      Y1              =   480
      Y2              =   480
   End
   Begin VB.Label lbl_cav 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   5
      Top             =   120
      Width           =   1935
   End
   Begin VB.Line Line5 
      X1              =   0
      X2              =   4440
      Y1              =   0
      Y2              =   0
   End
   Begin VB.Line Line4 
      X1              =   4440
      X2              =   4440
      Y1              =   0
      Y2              =   2880
   End
   Begin VB.Line Line3 
      X1              =   0
      X2              =   0
      Y1              =   2880
      Y2              =   0
   End
   Begin VB.Line Line2 
      X1              =   0
      X2              =   4440
      Y1              =   2880
      Y2              =   2880
   End
   Begin VB.Line Line1 
      X1              =   0
      X2              =   4440
      Y1              =   2280
      Y2              =   2280
   End
End
Attribute VB_Name = "Form2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub close_Click()

  End

End Sub

Private Sub new_Click()
   
   Me.Hide
   
   res(1).Text = 0
   res(2).Text = 0
   res(0).Text = 0
   lbl_cav(0).Caption = 0
   lbl_cav(1).Caption = 0
   
   Form1.cavallo(0).Left = 120
   Form1.cavallo(1).Left = 120
   Form1.cavallo(2).Left = 120
   Form1.cavallo(3).Left = 120
   
   Form1.txt_ordine(1).Text = ""
   Form1.txt_cavallo(0).Text = ""
   Form1.txt_euro(1).Text = ""
   
   Form1.txt_cavallo(0).Enabled = True
   Form1.txt_euro(1).Enabled = True
   Form1.cmd_gara.Enabled = True
   
   Form1.Enabled = True

End Sub
