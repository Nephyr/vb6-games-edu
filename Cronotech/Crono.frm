VERSION 5.00
Begin VB.Form Form1 
   BackColor       =   &H80000016&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Cronotech"
   ClientHeight    =   1215
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5415
   Icon            =   "Crono.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1215
   ScaleWidth      =   5415
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer T_Sec 
      Enabled         =   0   'False
      Interval        =   1
      Left            =   0
      Top             =   0
   End
   Begin VB.CommandButton Command2 
      Caption         =   "STOP"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   1
      Left            =   2760
      TabIndex        =   5
      Top             =   720
      Width           =   2535
   End
   Begin VB.CommandButton Command1 
      Caption         =   "START NOW"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   0
      Left            =   120
      TabIndex        =   4
      Top             =   720
      Width           =   2535
   End
   Begin VB.TextBox txt_crono 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   3
      Left            =   4080
      TabIndex        =   3
      Text            =   "0"
      Top             =   120
      Width           =   1215
   End
   Begin VB.TextBox txt_crono 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   2
      Left            =   2760
      TabIndex        =   2
      Text            =   "0"
      Top             =   120
      Width           =   1215
   End
   Begin VB.TextBox txt_crono 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   1
      Left            =   1440
      TabIndex        =   1
      Text            =   "0"
      Top             =   120
      Width           =   1215
   End
   Begin VB.TextBox txt_crono 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Text            =   "0"
      Top             =   120
      Width           =   1215
   End
   Begin VB.Line Line7 
      X1              =   3960
      X2              =   4080
      Y1              =   240
      Y2              =   240
   End
   Begin VB.Line Line6 
      X1              =   2640
      X2              =   2760
      Y1              =   240
      Y2              =   240
   End
   Begin VB.Line Line5 
      X1              =   1320
      X2              =   1440
      Y1              =   240
      Y2              =   240
   End
   Begin VB.Line Line4 
      X1              =   3960
      X2              =   4080
      Y1              =   360
      Y2              =   360
   End
   Begin VB.Line Line3 
      X1              =   2640
      X2              =   2760
      Y1              =   360
      Y2              =   360
   End
   Begin VB.Line Line2 
      X1              =   1320
      X2              =   1440
      Y1              =   360
      Y2              =   360
   End
   Begin VB.Line Line1 
      X1              =   120
      X2              =   5280
      Y1              =   600
      Y2              =   600
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Command1_Click(Index As Integer)
       
       If Command2(1).Caption = "RESET NOW" Then
       
          Command2(1).Enabled = True
          Command2(1).Caption = "STOP"
          T_Sec.Enabled = True
          Command1(0).Caption = "RESUME"
       
       Else
       
              
          Command1(0).Enabled = False
          T_Sec.Enabled = True
          Command2(1).Enabled = True
       
       End If
    
End Sub

Private Sub Command2_Click(Index As Integer)
       
       If Command2(1).Caption = "RESET NOW" Then
       
          Command2(1).Enabled = False
          txt_crono(3).Text = 0
          txt_crono(2).Text = 0
          txt_crono(1).Text = 0
          txt_crono(0).Text = 0
          Command1(0).Caption = "START NOW"
          Command2(1).Caption = "STOP"
       
       Else
       
          T_Sec.Enabled = False
          Command1(0).Enabled = True
          Command2(1).Caption = "RESET NOW"
          Command1(0).Caption = "RESUME"
       
       End If

End Sub

Private Sub T_Sec_Timer()

   txt_crono(3).Text = txt_crono(3).Text + 1
   
   If txt_crono(3).Text = 100 Then
   
      txt_crono(3).Text = 0
      txt_crono(2).Text = txt_crono(2).Text + 1
   
   End If
   
   If txt_crono(2).Text = 60 Then
   
      txt_crono(2).Text = 0
      txt_crono(1).Text = txt_crono(1).Text + 1
   
   End If
   
   If txt_crono(1).Text = 60 Then
   
      txt_crono(1).Text = 0
      txt_crono(0).Text = txt_crono(0).Text + 1
   
   End If

End Sub
