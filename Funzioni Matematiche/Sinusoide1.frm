VERSION 5.00
Begin VB.Form Form1 
   Caption         =   " Funzioni Goniometriche"
   ClientHeight    =   5235
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   8715
   LinkTopic       =   "Form1"
   ScaleHeight     =   1
   ScaleMode       =   0  'User
   ScaleWidth      =   8715
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox chk_tan 
      Caption         =   "Tangente"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   2
      Left            =   2400
      TabIndex        =   12
      Top             =   1920
      Width           =   1095
   End
   Begin VB.CheckBox chk_cos 
      Caption         =   "Coseno"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   1
      Left            =   1320
      TabIndex        =   11
      Top             =   1920
      Width           =   975
   End
   Begin VB.CheckBox chk_sen 
      Caption         =   "Seno"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   0
      Left            =   240
      TabIndex        =   10
      Top             =   1920
      Width           =   975
   End
   Begin VB.CommandButton txt_ric_teo 
      Caption         =   "Richiami Teorici"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   9
      Top             =   4560
      Width           =   3015
   End
   Begin VB.PictureBox obj_pict 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   5000
      Left            =   3600
      ScaleHeight     =   -5000
      ScaleMode       =   0  'User
      ScaleWidth      =   5000
      TabIndex        =   6
      Top             =   120
      Width           =   5000
      Begin VB.Label Label9 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "X = 5000"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   4215
         TabIndex        =   20
         Top             =   2280
         Width           =   780
      End
      Begin VB.Label Label8 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "X = 0"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   0
         TabIndex        =   19
         Top             =   2280
         Width           =   405
      End
      Begin VB.Label Label7 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Y = +2500"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   0
         TabIndex        =   18
         Top             =   0
         Width           =   4935
      End
      Begin VB.Label Label6 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Y = -2500"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   0
         TabIndex        =   17
         Top             =   4320
         Width           =   4935
      End
      Begin VB.Label Label4 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "SINUSOIDE"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   1560
         TabIndex        =   15
         Top             =   4648
         Width           =   1815
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "COSINUSOIDE"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   4648
         Width           =   1815
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "TANGENTOIDE"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000C000&
         Height          =   375
         Index           =   0
         Left            =   3120
         TabIndex        =   13
         Top             =   4648
         Width           =   1815
      End
      Begin VB.Shape Shape2 
         BackColor       =   &H8000000F&
         BackStyle       =   1  'Opaque
         Height          =   375
         Left            =   30
         Top             =   4560
         Width           =   4906
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Visualizza"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   5
      Top             =   4080
      Width           =   3000
   End
   Begin VB.TextBox txt_a 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   240
      TabIndex        =   4
      Top             =   960
      Width           =   3255
   End
   Begin VB.TextBox txt_b 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   240
      TabIndex        =   3
      Top             =   1560
      Width           =   3255
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Pulisci"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   2
      Top             =   3600
      Width           =   3015
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      Caption         =   "Esito "
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1095
      Left            =   240
      TabIndex        =   0
      Top             =   2160
      Width           =   3255
      Begin VB.TextBox txt_esito 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Left            =   120
         MultiLine       =   -1  'True
         TabIndex        =   1
         Top             =   240
         Width           =   3015
      End
   End
   Begin VB.Line Line1 
      X1              =   240
      X2              =   3480
      Y1              =   0.115
      Y2              =   0.115
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "FUNZIONI Sen, Cos, Tan"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   16
      Top             =   240
      Width           =   3015
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Valore A"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   240
      TabIndex        =   8
      Top             =   720
      Width           =   2535
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Valore B"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   240
      TabIndex        =   7
      Top             =   1320
      Width           =   2535
   End
   Begin VB.Shape Shape1 
      Height          =   3255
      Left            =   120
      Top             =   120
      Width           =   3495
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Command1_Click()

  obj_pict.ScaleMode = 1
  obj_pict.ScaleTop = -2500
  obj_pict.Cls
  
  Dim A, B As Single
  
  A = Val(txt_a.Text)
  B = Val(txt_b.Text)
  
  If ((B <> 0) And (A <> 0)) And ((B <> 0) Or (A <> 0)) Then
  
  obj_pict.Line (5000, 0)-(0, 0), &HC0C0C0
  
    If (chk_sen(0).Value = Unchecked) And (chk_tan(2).Value = Unchecked) And (chk_cos(1).Value = Unchecked) Then
  
       txt_esito.Text = "ATTENZIONE Nessuna funzione goniometrica selezionata!!"
  
    Else
    
        If chk_sen(0).Value <> Unchecked Then
       
          For Index = 1 To 5000
          obj_pict.PSet (Index, 0 - A * Sin((6.28 / B) * Index)), vbRed
          Next Index
          
        End If
        
        If chk_cos(1).Value <> Unchecked Then
       
          For Index = 1 To 5000
          obj_pict.PSet (Index, 0 - A * Cos((6.28 / B) * Index)), vbBlue
          Next Index
          
        End If
        
        If chk_tan(2).Value <> Unchecked Then
       
          For Index = 1 To 5000
          obj_pict.PSet (Index, 0 - A * Tan((6.28 / B) * Index)), &HC000&
          Next Index
          
        End If
        
        txt_esito.Text = "Completato con successo!!                 A = 1500 e B = 2480 valori consigliati."
    
    End If
  
  Else
  
    obj_pict.Line (5000, 0)-(0, 0), &HC0C0C0
    txt_esito.Text = "ERRORE I valori A e B non possono essere nulli o uguali a zero!!"
  
  End If

End Sub

Private Sub Command2_Click()

  obj_pict.Cls
  txt_a.Text = ""
  txt_b.Text = ""
  txt_esito.Text = ""

End Sub

Private Sub txt_ric_teo_Click()

   Form2.Show

End Sub

Private Sub Form_Unload(Cancel As Integer)
    Dim Risp As Integer
    Risp = MsgBox("Sei sicuro di voler chiudere il programma?", vbYesNo)
    If Risp = vbNo Then
        Cancel = True
    Else
        End
    End If
End Sub

Private Sub OnResize()
    Dim Risp As Integer
    Risp = MsgBox("Sei sicuro di voler chiudere il programma?", vbYesNo)
    If Risp = vbNo Then
        Cancel = True
    Else
        End
    End If
End Sub
