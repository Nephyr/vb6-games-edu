VERSION 5.00
Begin VB.Form Form1 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Funzione Circonferenza"
   ClientHeight    =   5220
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   8685
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5220
   ScaleWidth      =   8685
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      Caption         =   "Esito "
      ForeColor       =   &H80000008&
      Height          =   855
      Left            =   240
      TabIndex        =   11
      Top             =   2400
      Width           =   3255
      Begin VB.TextBox txt_esito 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   495
         Left            =   120
         MultiLine       =   -1  'True
         TabIndex        =   12
         Top             =   240
         Width           =   3015
      End
   End
   Begin VB.TextBox txt_y 
      Alignment       =   2  'Center
      Enabled         =   0   'False
      Height          =   285
      Left            =   2520
      TabIndex        =   10
      Top             =   3360
      Width           =   975
   End
   Begin VB.TextBox txt_x 
      Alignment       =   2  'Center
      Enabled         =   0   'False
      Height          =   285
      Left            =   240
      TabIndex        =   9
      Top             =   3360
      Width           =   975
   End
   Begin VB.TextBox txt_raggio 
      Alignment       =   2  'Center
      Enabled         =   0   'False
      Height          =   285
      Left            =   1320
      TabIndex        =   8
      Top             =   3720
      Width           =   1095
   End
   Begin VB.TextBox txt_c 
      Height          =   285
      Left            =   240
      TabIndex        =   7
      Top             =   1920
      Width           =   3255
   End
   Begin VB.TextBox txt_b 
      Height          =   285
      Left            =   240
      TabIndex        =   6
      Top             =   1200
      Width           =   3255
   End
   Begin VB.TextBox txt_a 
      Height          =   285
      Left            =   240
      TabIndex        =   3
      Top             =   480
      Width           =   3255
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Visualizza"
      Height          =   375
      Left            =   240
      TabIndex        =   2
      Top             =   4680
      Width           =   3255
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Pulisci"
      Height          =   375
      Left            =   240
      TabIndex        =   1
      Top             =   4200
      Width           =   3255
   End
   Begin VB.PictureBox obj_pict 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   5000
      Left            =   3600
      ScaleHeight     =   4965
      ScaleWidth      =   4965
      TabIndex        =   0
      Top             =   120
      Width           =   5000
   End
   Begin VB.Label Label1 
      Caption         =   "Valore C"
      Height          =   255
      Index           =   2
      Left            =   240
      TabIndex        =   14
      Top             =   1680
      Width           =   3255
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Caption         =   "X - r - Y"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1320
      TabIndex        =   13
      Top             =   3360
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Valore B"
      Height          =   255
      Index           =   1
      Left            =   240
      TabIndex        =   5
      Top             =   960
      Width           =   3255
   End
   Begin VB.Label Label1 
      Caption         =   "Valore A"
      Height          =   255
      Index           =   0
      Left            =   240
      TabIndex        =   4
      Top             =   240
      Width           =   3255
   End
   Begin VB.Shape Shape1 
      Height          =   3975
      Left            =   120
      Top             =   120
      Width           =   3495
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Function circo_X(a)

   circo_X = -(a / 2)

End Function

Private Function circo_Y(b)

   circo_Y = -(b / 2)

End Function

Private Function circo_R(a, b, c)

   circo_R = (Sqr(a ^ 2 + b ^ 2 - 4 * c)) / 2

End Function

Private Sub Command1_Click()
     
    obj_pict.Cls
    txt_x.Text = ""
    txt_y.Text = ""
    txt_raggio.Text = ""
    txt_a.Text = ""
    txt_b.Text = ""
    txt_c.Text = ""
    txt_esito.Text = ""
    
End Sub

Private Sub Command2_Click()

     Rem Dichiaro le variabili e ne assegno il valore del campo testuale
     Dim a, b, c, h, w, l, t, x, y, r, scaletemp As Single
     a = Val(txt_a.Text)
     b = Val(txt_b.Text)
     c = Val(txt_c.Text)
     
     Rem Pulisco il PictureBox
     obj_pict.Cls
     
     Rem Se pi� di due valori sono impostati a zero o sono alfabetici(Val = 0)
     If (a = 0 And b = 0 And c = 0) Or (a = 0 And b = 0) Then
       
       Rem Scrivo nel campo testuale l'errore commesso ed imposto a ZERO gli intervalli
       txt_esito.Text = "Errore: Impossibile calcolare con 2 o pi� valori nulli o alfabetici"
       txt_x.Text = 0
       txt_y.Text = 0
       txt_raggio.Text = 0
     
     Else Rem altrimenti proseguo
     
       Rem Assegno alle variabile i valori del PictureBox
       h = -5000
       w = 5000
       l = -2500
       t = 2500
     
       Rem Imposto il PictureBox in base alle assegnazioni precedenti
       obj_pict.ScaleMode = 0
       obj_pict.ScaleHeight = h
       obj_pict.ScaleWidth = w
       obj_pict.ScaleLeft = l
       obj_pict.ScaleTop = t
       
       Rem Traccio gli assi di riferimento X,Y
       obj_pict.Line (0, l)-(0, t), &HE0E0E0
       obj_pict.Line (l, 0)-(t, 0), &HE0E0E0
     
       Rem Disegno la circo. corrispondente ad A, B, C != 0
       If (a ^ 2 + b ^ 2 - 4 * c) > 0 Then
       
       Rem Assegno alla variabile X la funzione circo_X
       x = Fix(circo_X(a))
       Rem Assegno alla variabile Y la funzione circo_Y
       y = Fix(circo_Y(b))
       Rem Assegno alla variabile R la funzione circo_R
       r = Fix(circo_R(a, b, c))
       
       obj_pict.Circle (x, y), r, vbRed
       txt_x.Text = x
       txt_y.Text = y
       txt_raggio.Text = r
       
       End If
       
       If (a ^ 2 + b ^ 2 - 4 * c) = 0 Then
       x = -(a / 2)
       y = -(b / 2)
       obj_pict.PSet (x, y), vbRed
       txt_x.Text = x
       txt_y.Text = y
       txt_raggio.Text = "Null"
       End If
       
       If (a ^ 2 + b ^ 2 - 4 * c) < 0 Then
       txt_x.Text = "Null"
       txt_y.Text = "Null"
       txt_raggio.Text = "Null"
       End If
     
     
       Rem Se il calcolo � stato eseguito senza errori scrivo nel campo testuale
       txt_esito.Text = "Completato con successo"

     
     End If

End Sub

Private Sub Form_Unload(Cancel As Integer)
    Dim Risp As Integer
    Risp = MsgBox("Sei sicuro di voler chiudere il programma?", vbYesNo)
    If Risp = vbNo Then
        Cancel = True
    Else
        End
    End If
End Sub
