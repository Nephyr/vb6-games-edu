VERSION 5.00
Begin VB.Form Form1 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Click - Race"
   ClientHeight    =   3630
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6255
   Icon            =   "Click.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3630
   ScaleWidth      =   6255
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command3 
      Caption         =   "INDIETRO"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4320
      TabIndex        =   13
      Top             =   3120
      Width           =   1815
   End
   Begin VB.TextBox txt_count 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Index           =   1
      Left            =   120
      TabIndex        =   11
      Text            =   "0"
      Top             =   3120
      Width           =   975
   End
   Begin VB.TextBox txt_count 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Index           =   0
      Left            =   120
      TabIndex        =   5
      Text            =   "0"
      Top             =   2640
      Width           =   975
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   100
      Left            =   5880
      Top             =   0
   End
   Begin VB.CommandButton Command2 
      Caption         =   "GIOCA"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4320
      TabIndex        =   4
      Top             =   2640
      Width           =   1815
   End
   Begin VB.PictureBox Pic 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   1215
      Index           =   1
      Left            =   120
      ScaleHeight     =   1185
      ScaleWidth      =   5985
      TabIndex        =   3
      Top             =   1320
      Width           =   6015
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Giocatore"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   4920
         TabIndex        =   9
         Top             =   0
         Width           =   975
      End
   End
   Begin VB.PictureBox Pic 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   1215
      Index           =   0
      Left            =   120
      ScaleHeight     =   1185
      ScaleWidth      =   5985
      TabIndex        =   2
      Top             =   120
      Width           =   6015
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Computer"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   4920
         TabIndex        =   10
         Top             =   960
         Width           =   975
      End
   End
   Begin VB.Frame Frame 
      Appearance      =   0  'Flat
      Caption         =   "Seleziona la Difficoltà"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1815
      Left            =   720
      TabIndex        =   0
      Top             =   360
      Visible         =   0   'False
      Width           =   4695
      Begin VB.TextBox txt_dif 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   360
         TabIndex        =   7
         Text            =   "1"
         Top             =   600
         Width           =   3975
      End
      Begin VB.CommandButton Command1 
         Caption         =   "VAI AL GIOCO..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   360
         TabIndex        =   1
         Top             =   1080
         Width           =   3975
      End
      Begin VB.Label Label2 
         Caption         =   "Scrivi il livello di difficoltà da: 1 a 5"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   360
         TabIndex        =   8
         Top             =   360
         Width           =   3975
      End
   End
   Begin VB.Label Label 
      Caption         =   "Km percorsi dal giocatore..."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1200
      TabIndex        =   12
      Top             =   3240
      Width           =   3015
   End
   Begin VB.Label Label1 
      Caption         =   "Km percorsi dal computer..."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1200
      TabIndex        =   6
      Top             =   2760
      Width           =   3015
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()

 If (Val(txt_dif.Text) <> 0) And (txt_dif.Text <= 5) And (txt_dif.Text >= 1) Then
 
   Frame.Visible = False
   
   If txt_dif.Text = 1 Then
   
      Timer1.Interval = 200
   
   End If
   
   If txt_dif.Text = 2 Then
   
      Timer1.Interval = 150
   
   End If
   
   If txt_dif.Text = 3 Then
   
      Timer1.Interval = 130
   
   End If
   
   If txt_dif.Text = 4 Then
   
      Timer1.Interval = 100
   
   End If
   
   If txt_dif.Text = 5 Then
   
      Timer1.Interval = 50
   
   End If
   
   Pic(1).Visible = True
   Pic(0).Visible = True
   Command2.Visible = True
   Command3.Visible = True
   txt_count(0).Visible = True
   txt_count(1).Visible = True
   Label1.Visible = True
   Label.Visible = True
   
 End If

End Sub

Private Sub Command2_Click()
   
   If Command2.Caption = "GIOCA" Then

      Timer1.Enabled = True
      Command2.Caption = "CORRI"
      Command2.Default = True
      Command3.Enabled = False
      Counter_Click = 0
   
   Else
   
      If txt_count(1).Text = 6000 Then
      
         Form2.txt_result = "HAI VINTO... CONGRATULAZIONI!!"
         Form2.Show
         Me.Hide
         
         Timer1.Enabled = False
         Command2.Caption = "GIOCA"
         
         Pic(1).Visible = False
         Pic(0).Visible = False
         Command2.Visible = False
         Command3.Visible = False
         txt_count(1).Visible = False
         txt_count(0).Visible = False
         Label1.Visible = False
         Label.Visible = False
         Frame.Visible = True
         txt_count(1).Text = 0
         txt_count(0).Text = 0
         
         Pic(1).DrawWidth = 1
         Pic(1).Line (0, -607.5)-(0, 607.5)
         Pic(1).Line (0, 1)-(6000, 1), vbGreen
         Pic(1).Cls
      
      Else
      
         Pic(1).DrawWidth = 1
         Pic(1).Line (0, -607.5)-(0, 607.5)
         Pic(1).Line (0, 1)-(6000, 1), vbGreen
   
         Pic(1).DrawWidth = 3
         Pic(1).Line (txt_count(1).Text, 0)-(txt_count(1).Text + 100, 0)
         txt_count(1).Text = txt_count(1).Text + 100
      
      End If
   
   End If

End Sub

Private Sub Command3_Click()

   Pic(1).Visible = False
   Pic(0).Visible = False
   Command2.Visible = False
   Command3.Visible = False
   txt_count(0).Visible = False
   txt_count(1).Visible = False
   Label1.Visible = False
   Label.Visible = False
   Frame.Visible = True
   
   Pic(0).ScaleMode = 1
   Pic(0).ScaleTop = -607.5
   Pic(0).ScaleLeft = 15
   Pic(0).Cls
   Pic(1).ScaleMode = 1
   Pic(1).ScaleTop = -607.5
   Pic(1).ScaleLeft = 15
   Pic(1).Cls
   
   Timer1.Enabled = False
   
   txt_count(1).Text = 0
   txt_count(0).Text = 0

End Sub

Private Sub Form_Load()

   Pic(1).Visible = False
   Pic(0).Visible = False
   Command2.Visible = False
   Command3.Visible = False
   txt_count(0).Visible = False
   txt_count(1).Visible = False
   Label1.Visible = False
   Label.Visible = False
   Frame.Visible = True
   
   Pic(0).ScaleMode = 1
   Pic(0).ScaleTop = -607.5
   Pic(0).ScaleLeft = 15
   Pic(0).Cls
   Pic(1).ScaleMode = 1
   Pic(1).ScaleTop = -607.5
   Pic(1).ScaleLeft = 15
   Pic(1).Cls

End Sub

Private Sub Timer1_Timer()
      
   Pic(0).DrawWidth = 1
   Pic(0).Line (0, -607.5)-(0, 607.5)
   Pic(0).Line (0, 1)-(6000, 1), vbGreen
   
   If txt_count(0).Text = 6000 Then
   
      Form2.txt_result = "IL COMPUTER HA VINTO... RIPROVA!!"
      Form2.Show
      Me.Hide
      
      Timer1.Enabled = False
      txt_count(0).Text = 0
      txt_count(1).Text = 0
      Command2.Caption = "GIOCA"
      
      Pic(0).DrawWidth = 1
      Pic(0).Line (0, -607.5)-(0, 607.5)
      Pic(0).Line (0, 1)-(6000, 1), vbGreen
      Pic(0).Cls
   
   Else
   
      Pic(0).DrawWidth = 3
      Pic(0).Line (txt_count(0).Text, 0)-(txt_count(0).Text + 100, 0)
      txt_count(0).Text = txt_count(0).Text + 100
   
   End If

End Sub
