VERSION 5.00
Begin VB.Form Form1 
   BackColor       =   &H80000004&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Ordinatore Alfabetico"
   ClientHeight    =   4935
   ClientLeft      =   45
   ClientTop       =   405
   ClientWidth     =   8295
   ForeColor       =   &H00000000&
   Icon            =   "Alpha.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "Alpha.frx":8934
   ScaleHeight     =   4935
   ScaleWidth      =   8295
   StartUpPosition =   2  'CenterScreen
   WhatsThisButton =   -1  'True
   WhatsThisHelp   =   -1  'True
   Begin VB.CheckBox Check1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Seleziona tutto"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   5820
      TabIndex        =   13
      ToolTipText     =   " Spunta per selezionare tutta la lista "
      Top             =   1920
      Width           =   2415
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Informazione"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1455
      Left            =   5820
      TabIndex        =   10
      Top             =   2760
      Width           =   2355
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Per eliminare un nome selezionarlo tramite il quadrato alla sua sinistra e cliccare il tasto a fondo pagina..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1095
         Left            =   90
         TabIndex        =   11
         Top             =   240
         Width           =   2175
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Cancella Selezionati"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5760
      TabIndex        =   2
      ToolTipText     =   " Cancella dall'elenco i Nomi selezionati "
      Top             =   4440
      Width           =   2445
   End
   Begin VB.ListBox List1 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3810
      ItemData        =   "Alpha.frx":19DA6
      Left            =   0
      List            =   "Alpha.frx":19DA8
      Sorted          =   -1  'True
      Style           =   1  'Checkbox
      TabIndex        =   1
      Top             =   520
      Width           =   5655
   End
   Begin VB.TextBox Text 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Index           =   0
      Left            =   600
      TabIndex        =   0
      ToolTipText     =   " Immettere il Nome numero: 0 "
      Top             =   120
      Width           =   5775
   End
   Begin VB.Image Image1 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   300
      Left            =   240
      MouseIcon       =   "Alpha.frx":19DAA
      MousePointer    =   99  'Custom
      Picture         =   "Alpha.frx":1A0B4
      ToolTipText     =   " Esporta lista in un file testuale "
      Top             =   120
      Width           =   300
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "N� Nomi: 5"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5860
      TabIndex        =   19
      Top             =   1200
      Width           =   2175
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   "N� Nomi: 10"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5860
      TabIndex        =   18
      Top             =   1440
      Width           =   2175
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   "N� Nomi: 15"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5860
      TabIndex        =   17
      Top             =   1680
      Width           =   2175
   End
   Begin VB.Label Label8 
      BackStyle       =   0  'Transparent
      Caption         =   "N� Nomi: 20"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5860
      TabIndex        =   16
      Top             =   1920
      Width           =   2175
   End
   Begin VB.Label Label9 
      BackStyle       =   0  'Transparent
      Caption         =   "N� Nomi: 25"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5860
      TabIndex        =   15
      Top             =   2160
      Width           =   2175
   End
   Begin VB.Label Label10 
      BackStyle       =   0  'Transparent
      Caption         =   "N� Nomi: 30"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5860
      TabIndex        =   14
      Top             =   2400
      Width           =   2175
   End
   Begin VB.Shape Shape2 
      BackStyle       =   1  'Opaque
      Height          =   1425
      Left            =   5820
      Top             =   1200
      Width           =   2340
   End
   Begin VB.Label Vis 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "+"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   7800
      TabIndex        =   12
      ToolTipText     =   " Clicca per visualizzare il menu: Inserimenti "
      Top             =   960
      Width           =   360
   End
   Begin VB.Label Label5 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "Numero Inserito nella lista"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5880
      TabIndex        =   9
      Top             =   1320
      Width           =   2295
   End
   Begin VB.Label Incr 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Index           =   1
      Left            =   5820
      TabIndex        =   8
      ToolTipText     =   " Numero Nomi inseriti"
      Top             =   1560
      Width           =   2340
   End
   Begin VB.Label Label4 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "Numero Massimo Inseribile"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5880
      TabIndex        =   7
      Top             =   720
      Width           =   2295
   End
   Begin VB.Label Incr 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "5"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Index           =   0
      Left            =   5820
      TabIndex        =   6
      ToolTipText     =   " Numero Nomi inseribili"
      Top             =   960
      Width           =   1995
   End
   Begin VB.Shape Shape1 
      BackStyle       =   1  'Opaque
      Height          =   3810
      Left            =   5690
      Top             =   525
      Width           =   2610
   End
   Begin VB.Label Command2 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Aggiungi Nome"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6480
      MouseIcon       =   "Alpha.frx":1A4E6
      MousePointer    =   99  'Custom
      TabIndex        =   5
      ToolTipText     =   " Premi il Tasto ENTER, o clicca qui, per inserire "
      Top             =   150
      Width           =   1695
   End
   Begin VB.Label Elim 
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   2400
      TabIndex        =   4
      Top             =   4515
      Width           =   615
   End
   Begin VB.Label Label2 
      BackColor       =   &H80000004&
      Caption         =   "Numero Nomi selezionati:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   4520
      Width           =   3015
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Function Insert_Into(type_exe, num_index)

    Rem Aggiungo, nel caso le condizioni siano verificate, il nome immesso nel listbox.
  
    If (Text(0).Text <> "") And (Not IsNumeric(Text(0).Text)) Then
       
       If Incr(0).Caption = Incr(1).Caption Then
         tmp = MsgBox(" ATTENZIONE" & vbCrLf & " Massimo inseribile raggiunto!! ", vbCritical)
       Else
         
         Rem divido la stringa in due
         Str_Name_Split = Split(LTrim(RTrim(Text(0).Text)), " ")
         
         Dim Iniz_str(100), Body_str(100) As String
         
         For i = 0 To UBound(Str_Name_Split) Step 1
           Rem Rendo maiuscola la prima lettera e tolgo eventuali spazi bianchi
           Iniz_str(i) = UCase(Mid(Str_Name_Split(i), 1, 1))
           Body_str(i) = LCase(Mid(Str_Name_Split(i), 2))
         Next
         
         For i = 0 To UBound(Str_Name_Split) Step 1
           Rem Rendo maiuscola la prima lettera e tolgo eventuali spazi bianchi
           Mod_Str = Mod_Str & Iniz_str(i) & Body_str(i) & " "
         Next
         
         Rem Inserisco la parola ricomposta nel ListBox
         If type_exe = "single" Then
           List1.AddItem Mod_Str & " (1)"
           Incr(1).Caption = Incr(1).Caption + 1
           Text(0).ToolTipText = " Immettere il Nome numero: " & Incr(1).Caption & " "
         Else
           val_name_temp = Mid(Mid(List1.List(num_index), Len(List1.List(num_index)) - 1), 1, 1)
           val_name = val_name_temp + 1
           List1.RemoveItem num_index
           List1.AddItem Mod_Str & " (" & val_name & ")"
           Incr(1).Caption = Incr(1).Caption + 1
           Text(0).ToolTipText = " Immettere il Nome numero: " & Incr(1).Caption & " "
         End If
       
       End If
    
    Else
      tmp = MsgBox(" ATTENZIONE" & vbCrLf & " Inserire nomi Alfabetici!! ", vbCritical)
    End If
  
    Text(0).Text = ""

End Function

Function Controlla()

    Dim num_rec As Integer
    For i = 0 To Form1.List1.ListCount - 1 Step 1
     If LCase(Trim(Text(0).Text)) = LCase(Trim(Mid(Form1.List1.List(i), 1, Len(Form1.List1.List(i)) - 3))) Then
     
       num_rec = num_rec + 1
       num_index = i
     
     End If
    Next
    
    If num_rec = 0 Then
    
     Call Insert_Into("single", "ND")
      
    Else
      
     val_name_err = Int(Mid(Mid(List1.List(num_index), Len(List1.List(num_index)) - 1), 1, 1))
     Resp = MsgBox(" ATTENZIONE" & vbCrLf & " Il nome immesso risulta gi� presente nella lista! " & vbCrLf & " Riscontrati: " & val_name_err, vbOKOnly + vbInformation)
     If Resp = vbOK Then
       
       Resp = MsgBox(" Inserire comunque il Nome immesso? ", vbYesNo + vbQuestion)
       If Resp = vbYes Then
         Call Insert_Into("multiple", num_index)
       Else
         Text(0).Text = ""
       End If
       
     End If
    
    End If

End Function

Private Sub Check1_Click()

  If Check1.Value = 1 Then
    For i = List1.ListCount - 1 To 0 Step -1
     List1.Selected(i) = True
     List1.Enabled = False
    Next
  Else
    For i = List1.ListCount - 1 To 0 Step -1
     List1.Selected(i) = False
     List1.Enabled = True
    Next
  End If
  
End Sub

Private Sub Command1_Click()

  Rem Elimina i nomi selezionati nella lista.
  
  If Check1.Value = 0 Then
  
    For i = List1.ListCount - 1 To 0 Step -1
     If List1.Selected(i) = True Then
       List1.RemoveItem (i)
       Incr(1).Caption = Incr(1).Caption - Elim.Caption
     End If
    Next
    Elim.Caption = 0
    List1.Enabled = True
    Check1.Value = 0
    
  Else
    
    For i = List1.ListCount - 1 To 0 Step -1
     If List1.Selected(i) = True Then
       List1.RemoveItem (i)
     End If
    Next
    Incr(1).Caption = 0
    Elim.Caption = 0
    Check1.Value = 0
    List1.Enabled = True
    
  End If
 
End Sub

Private Sub Command2_Click()

    Call Controlla

End Sub

Private Sub Form_Load()

    Shape2.Visible = False
    Label3.Visible = False
    Label6.Visible = False
    Label7.Visible = False
    Label8.Visible = False
    Label9.Visible = False
    Label10.Visible = False
    Vis.Caption = "+"

End Sub

Private Sub Image1_Click()

    Dialog.Show

End Sub

Private Sub Label10_Click()

    Incr(0).Caption = 30
    Label3.Visible = False
    Label6.Visible = False
    Label7.Visible = False
    Label8.Visible = False
    Label9.Visible = False
    Label10.Visible = False
    Shape2.Visible = False
    Vis.Caption = "+"
    Check1.Visible = True

End Sub

Private Sub Label3_Click()

    Incr(0).Caption = 5
    Label3.Visible = False
    Label6.Visible = False
    Label7.Visible = False
    Label8.Visible = False
    Label9.Visible = False
    Label10.Visible = False
    Shape2.Visible = False
    Vis.Caption = "+"
    Check1.Visible = True

End Sub

Private Sub Label6_Click()

    Incr(0).Caption = 10
    Label3.Visible = False
    Label6.Visible = False
    Label7.Visible = False
    Label8.Visible = False
    Label9.Visible = False
    Label10.Visible = False
    Shape2.Visible = False
    Vis.Caption = "+"
    Check1.Visible = True

End Sub

Private Sub Label7_Click()

    Incr(0).Caption = 15
    Label3.Visible = False
    Label6.Visible = False
    Label7.Visible = False
    Label8.Visible = False
    Label9.Visible = False
    Label10.Visible = False
    Shape2.Visible = False
    Vis.Caption = "+"
    Check1.Visible = True

End Sub

Private Sub Label8_Click()

    Incr(0).Caption = 20
    Label3.Visible = False
    Label6.Visible = False
    Label7.Visible = False
    Label8.Visible = False
    Label9.Visible = False
    Label10.Visible = False
    Shape2.Visible = False
    Vis.Caption = "+"
    Check1.Visible = True

End Sub

Private Sub Label9_Click()

    Incr(0).Caption = 25
    Label3.Visible = False
    Label6.Visible = False
    Label7.Visible = False
    Label8.Visible = False
    Label9.Visible = False
    Label10.Visible = False
    Shape2.Visible = False
    Vis.Caption = "+"
    Check1.Visible = True

End Sub

Private Sub Text_KeyPress(Index As Integer, KeyAscii As Integer)

  Rem Se premi ENTER aggiungo il nome alla lista
  If KeyAscii = 13 Then
    
    Call Controlla
    
  End If
  
End Sub

Private Sub Vis_Click()

  If Shape2.Visible = False Then
    Shape2.Visible = True
    Label3.Visible = True
    Label6.Visible = True
    Label7.Visible = True
    Label8.Visible = True
    Label9.Visible = True
    Label10.Visible = True
    Vis.Caption = "-"
    Check1.Visible = False
  Else
    Shape2.Visible = False
    Label3.Visible = False
    Label6.Visible = False
    Label7.Visible = False
    Label8.Visible = False
    Label9.Visible = False
    Label10.Visible = False
    Vis.Caption = "+"
    Check1.Visible = True
  End If

End Sub

Private Sub List1_Click()

  Rem Conta i nomi selezionati nella lista.
  
  Elim.Caption = 0
  For i = List1.ListCount - 1 To 0 Step -1
    If List1.Selected(i) = True Then
      val_name_temp = Int(Mid(Mid(List1.List(i), Len(List1.List(i)) - 1), 1, 1))
      Elim.Caption = Elim.Caption + val_name_temp
    End If
  Next

End Sub

Private Sub Form_Unload(Cancel As Integer)
    Dim Risp As Integer
    Risp = MsgBox("Sei sicuro di voler chiudere il programma?", vbYesNo + vbQuestion)
    If Risp = vbNo Then
        Cancel = True
    Else
        End
    End If
End Sub
