VERSION 5.00
Begin VB.Form Form1 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Num. Random"
   ClientHeight    =   3495
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   4455
   Icon            =   "Slot.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   MouseIcon       =   "Slot.frx":2CC07
   Picture         =   "Slot.frx":2CF11
   ScaleHeight     =   3495
   ScaleWidth      =   4455
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txt 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   48
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Index           =   3
      Left            =   3360
      TabIndex        =   8
      Text            =   "0"
      Top             =   120
      Width           =   975
   End
   Begin VB.TextBox txt_cred 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2400
      TabIndex        =   5
      Text            =   "100"
      Top             =   2640
      Width           =   735
   End
   Begin VB.TextBox txt_esito 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   240
      TabIndex        =   4
      Text            =   "Clicca sul tasto INSERISCI e GIOCA"
      Top             =   1560
      Width           =   3975
   End
   Begin VB.PictureBox btn_gioca 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   185
      MouseIcon       =   "Slot.frx":594B3
      MousePointer    =   99  'Custom
      Picture         =   "Slot.frx":597BD
      ScaleHeight     =   225
      ScaleWidth      =   4065
      TabIndex        =   3
      Top             =   3120
      Width           =   4095
   End
   Begin VB.TextBox txt 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   48
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Index           =   2
      Left            =   2280
      TabIndex        =   2
      Text            =   "0"
      Top             =   120
      Width           =   975
   End
   Begin VB.TextBox txt 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   48
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1200
      Index           =   1
      Left            =   1200
      TabIndex        =   1
      Text            =   "0"
      Top             =   120
      Width           =   975
   End
   Begin VB.TextBox txt 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   48
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1200
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Text            =   "0"
      Top             =   120
      Width           =   975
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "-   di 6000"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   255
      Left            =   3240
      TabIndex        =   7
      Top             =   2640
      Width           =   855
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "I Tuoi Crediti Residui   -"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   255
      Index           =   0
      Left            =   360
      TabIndex        =   6
      Top             =   2640
      Width           =   1935
   End
   Begin VB.Line Line1 
      X1              =   240
      X2              =   4200
      Y1              =   2520
      Y2              =   2520
   End
   Begin VB.Shape Shape1 
      BackStyle       =   1  'Opaque
      Height          =   1575
      Left            =   120
      Top             =   1440
      Width           =   4215
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub btn_gioca_Click()

   Dim x1, x2, x3, crediti As Integer
   x1 = Rnd() * 9 + 1
   x2 = Rnd() * 9 + 1
   x3 = Rnd() * 9 + 1
   x4 = Rnd() * 9 + 1
   crediti = Val(txt_cred.Text)
   crediti = crediti - 3
   txt_cred.Text = crediti
   
   txt(0).Text = Fix(x1)
   txt(1).Text = Fix(x2)
   txt(2).Text = Fix(x3)
   txt(3).Text = Fix(x4)
   
   If (Fix(x1) = Fix(x2)) And (Fix(x2) = Fix(x3)) And (Fix(x1) = Fix(x3)) And (Fix(x3) = Fix(x4)) Then
   txt_esito.Text = "CHECKPOT!!!! Hai Vinto 500 Crediti!!!!"
   crediti = crediti + 500
   txt_cred.Text = crediti
   Else
   
     If ((Fix(x1) = Fix(x2)) And (Fix(x2) = Fix(x3))) Or ((Fix(x1) = Fix(x2)) And (Fix(x2) = Fix(x4))) Or ((Fix(x1) = Fix(x3)) And (Fix(x3) = Fix(x4))) Or ((Fix(x2) = Fix(x3)) And (Fix(x3) = Fix(x4))) Then
     txt_esito.Text = "TRIS!! Hai Vinto 250 Crediti!!"
     crediti = crediti + 250
     txt_cred.Text = crediti
     Else
     
        If (Fix(x1) = Fix(x2)) Or (Fix(x2) = Fix(x3)) Or (Fix(x1) = Fix(x3)) Or (Fix(x1) = Fix(x4)) Or (Fix(x2) = Fix(x4)) Or (Fix(x3) = Fix(x4)) Then
         
          If ((Fix(x1) = Fix(x2)) And (Fix(x3) = Fix(x4))) Or ((Fix(x1) = Fix(x4)) And (Fix(x2) = Fix(x3))) Then
            
            txt_esito.Text = "DOPPIA COPPIA!! Hai Vinto 20 Crediti!!"
            crediti = crediti + 20
            txt_cred.Text = crediti
          
          Else
          
            txt_esito.Text = "COPPIA!! Hai Vinto 10 Crediti!!"
            crediti = crediti + 10
            txt_cred.Text = crediti
            
          End If
        
        Else
        txt_esito.Text = "Mi dispiace hai perso... Ritenta"
        End If
     
     End If
   
   End If
   
   If crediti >= 6000 Then
   
      Form2.Show
      
      crediti = 100
      txt_cred.Text = ""
      txt_esito.Text = "HAI VINTO... Crediti ripristinati. RIGIOCA!!"
   
   End If

End Sub

Private Sub btn_now_Click()

End Sub

