VERSION 5.00
Begin VB.Form Form1 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "TEST REAZIONE"
   ClientHeight    =   2415
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   4020
   Icon            =   "Test.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2415
   ScaleWidth      =   4020
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      Caption         =   "RISULTATO"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1695
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   2535
      Begin VB.TextBox txt_esito 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1335
         Left            =   120
         MultiLine       =   -1  'True
         TabIndex        =   2
         Text            =   "Test.frx":18175
         Top             =   240
         Width           =   2295
      End
   End
   Begin VB.Timer Timer 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   3600
      Top             =   0
   End
   Begin VB.CommandButton Command 
      Caption         =   "GIOCA"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   1920
      Width           =   2535
   End
   Begin VB.Shape shape 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00000000&
      Height          =   975
      Index           =   1
      Left            =   2760
      Shape           =   5  'Rounded Square
      Top             =   120
      Width           =   1095
   End
   Begin VB.Shape shape 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00000000&
      Height          =   975
      Index           =   0
      Left            =   2640
      Shape           =   5  'Rounded Square
      Top             =   1320
      Width           =   1335
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Command_Click()

   If Command.Caption = "GIOCA" Then
   
      Timer.Enabled = True
      Command.Caption = "FERMA ORA"
   
   Else
   
      Timer.Enabled = False
      Command.Caption = "GIOCA"
      
      If (shape(0).BackColor = vbWhite) And (shape(1).BackColor = vbWhite) Then
      
         txt_esito.Text = "Mi dispiace hai perso... Non sei abbastanza veloce."
      
      Else
      
         txt_esito.Text = "Congratulationi hai vinto!!!!! Velocit� di reazione: " & Timer.Interval & " ms."
      
      End If
   
   End If

End Sub

Private Sub Timer_Timer()

   NUM = Fix(Rnd() * 20 + 1)
   
   If NUM = 5 Then
   
      shape(0).BackColor = vbGreen
      Timer.Interval = NUM * 20
   
   ElseIf NUM = 10 Then
   
      shape(1).BackColor = vbGreen
      shape(0).BackColor = vbWhite
      Timer.Interval = NUM + 50
   
   ElseIf NUM = 16 Then
   
      shape(0).BackColor = vbGreen
      shape(1).BackColor = vbWhite
      Timer.Interval = NUM * 10
   
   ElseIf NUM = 2 Then
   
      shape(1).BackColor = vbGreen
      shape(1).BackColor = vbWhite
      Timer.Interval = NUM * 100
   
   ElseIf NUM = 20 Then
   
      shape(1).BackColor = vbGreen
      shape(1).BackColor = vbWhite
      Timer.Interval = NUM * 2
   
   Else
   
      shape(0).BackColor = vbWhite
      shape(1).BackColor = vbWhite
      Timer.Interval = NUM + 200
   
   End If

End Sub
