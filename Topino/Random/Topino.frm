VERSION 5.00
Begin VB.Form Form1 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "  Simulazione Topo"
   ClientHeight    =   8190
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7830
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "Topino.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   Picture         =   "Topino.frx":F792
   ScaleHeight     =   8190
   ScaleWidth      =   7830
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox Direzione 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1800
      TabIndex        =   2
      Text            =   "..."
      Top             =   7800
      Width           =   1215
   End
   Begin VB.TextBox FBack 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   4200
      TabIndex        =   1
      Text            =   "0"
      Top             =   7800
      Width           =   975
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Avvia Simulazione"
      Height          =   255
      Left            =   5280
      TabIndex        =   0
      Top             =   7800
      Width           =   2415
   End
   Begin VB.Timer Timer1 
      Left            =   7395
      Top             =   0
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   " - Feedback - "
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3000
      TabIndex        =   4
      Top             =   7800
      Width           =   1215
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Direzione del topo -"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   7800
      Width           =   1695
   End
   Begin VB.Image Image2 
      Height          =   375
      Left            =   6600
      Picture         =   "Topino.frx":16D2A4
      Top             =   840
      Width           =   360
   End
   Begin VB.Image Image1 
      Height          =   615
      Left            =   0
      Picture         =   "Topino.frx":16D725
      Stretch         =   -1  'True
      Top             =   7200
      Width           =   615
   End
   Begin VB.Shape Shape101 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   6960
      Shape           =   5  'Rounded Square
      Top             =   6960
      Width           =   375
   End
   Begin VB.Shape Shape100 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   6240
      Shape           =   5  'Rounded Square
      Top             =   6960
      Width           =   375
   End
   Begin VB.Shape Shape99 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   5520
      Shape           =   5  'Rounded Square
      Top             =   6960
      Width           =   375
   End
   Begin VB.Shape Shape98 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   4800
      Shape           =   5  'Rounded Square
      Top             =   6960
      Width           =   375
   End
   Begin VB.Shape Shape97 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   4080
      Shape           =   5  'Rounded Square
      Top             =   6960
      Width           =   375
   End
   Begin VB.Shape Shape96 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   3360
      Shape           =   5  'Rounded Square
      Top             =   6960
      Width           =   375
   End
   Begin VB.Shape Shape95 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   2640
      Shape           =   5  'Rounded Square
      Top             =   6960
      Width           =   375
   End
   Begin VB.Shape Shape94 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   1920
      Shape           =   5  'Rounded Square
      Top             =   6960
      Width           =   375
   End
   Begin VB.Shape Shape93 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   1200
      Shape           =   5  'Rounded Square
      Top             =   6960
      Width           =   375
   End
   Begin VB.Shape Shape92 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   480
      Shape           =   5  'Rounded Square
      Top             =   6960
      Width           =   375
   End
   Begin VB.Shape Shape91 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   6960
      Shape           =   5  'Rounded Square
      Top             =   6240
      Width           =   375
   End
   Begin VB.Shape Shape90 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   6240
      Shape           =   5  'Rounded Square
      Top             =   6240
      Width           =   375
   End
   Begin VB.Shape Shape89 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   5520
      Shape           =   5  'Rounded Square
      Top             =   6240
      Width           =   375
   End
   Begin VB.Shape Shape88 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   4800
      Shape           =   5  'Rounded Square
      Top             =   6240
      Width           =   375
   End
   Begin VB.Shape Shape87 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   4080
      Shape           =   5  'Rounded Square
      Top             =   6240
      Width           =   375
   End
   Begin VB.Shape Shape86 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   3360
      Shape           =   5  'Rounded Square
      Top             =   6240
      Width           =   375
   End
   Begin VB.Shape Shape85 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   2640
      Shape           =   5  'Rounded Square
      Top             =   6240
      Width           =   375
   End
   Begin VB.Shape Shape84 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   1920
      Shape           =   5  'Rounded Square
      Top             =   6240
      Width           =   375
   End
   Begin VB.Shape Shape83 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   1200
      Shape           =   5  'Rounded Square
      Top             =   6240
      Width           =   375
   End
   Begin VB.Shape Shape82 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   480
      Shape           =   5  'Rounded Square
      Top             =   6240
      Width           =   375
   End
   Begin VB.Shape Shape81 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   6960
      Shape           =   5  'Rounded Square
      Top             =   5520
      Width           =   375
   End
   Begin VB.Shape Shape80 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   6240
      Shape           =   5  'Rounded Square
      Top             =   5520
      Width           =   375
   End
   Begin VB.Shape Shape79 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   5520
      Shape           =   5  'Rounded Square
      Top             =   5520
      Width           =   375
   End
   Begin VB.Shape Shape78 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   4800
      Shape           =   5  'Rounded Square
      Top             =   5520
      Width           =   375
   End
   Begin VB.Shape Shape77 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   4080
      Shape           =   5  'Rounded Square
      Top             =   5520
      Width           =   375
   End
   Begin VB.Shape Shape76 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   3360
      Shape           =   5  'Rounded Square
      Top             =   5520
      Width           =   375
   End
   Begin VB.Shape Shape75 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   2640
      Shape           =   5  'Rounded Square
      Top             =   5520
      Width           =   375
   End
   Begin VB.Shape Shape74 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   1920
      Shape           =   5  'Rounded Square
      Top             =   5520
      Width           =   375
   End
   Begin VB.Shape Shape73 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   1200
      Shape           =   5  'Rounded Square
      Top             =   5520
      Width           =   375
   End
   Begin VB.Shape Shape72 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   480
      Shape           =   5  'Rounded Square
      Top             =   5520
      Width           =   375
   End
   Begin VB.Shape Shape71 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   6960
      Shape           =   5  'Rounded Square
      Top             =   480
      Width           =   375
   End
   Begin VB.Shape Shape70 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   6240
      Shape           =   5  'Rounded Square
      Top             =   480
      Width           =   375
   End
   Begin VB.Shape Shape69 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   5520
      Shape           =   5  'Rounded Square
      Top             =   480
      Width           =   375
   End
   Begin VB.Shape Shape68 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   4800
      Shape           =   5  'Rounded Square
      Top             =   480
      Width           =   375
   End
   Begin VB.Shape Shape67 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   4080
      Shape           =   5  'Rounded Square
      Top             =   480
      Width           =   375
   End
   Begin VB.Shape Shape66 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   3360
      Shape           =   5  'Rounded Square
      Top             =   480
      Width           =   375
   End
   Begin VB.Shape Shape65 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   2640
      Shape           =   5  'Rounded Square
      Top             =   480
      Width           =   375
   End
   Begin VB.Shape Shape64 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   1920
      Shape           =   5  'Rounded Square
      Top             =   480
      Width           =   375
   End
   Begin VB.Shape Shape63 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   1200
      Shape           =   5  'Rounded Square
      Top             =   480
      Width           =   375
   End
   Begin VB.Shape Shape62 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   480
      Shape           =   5  'Rounded Square
      Top             =   480
      Width           =   375
   End
   Begin VB.Shape Shape61 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   6960
      Shape           =   5  'Rounded Square
      Top             =   1200
      Width           =   375
   End
   Begin VB.Shape Shape60 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   6240
      Shape           =   5  'Rounded Square
      Top             =   1200
      Width           =   375
   End
   Begin VB.Shape Shape59 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   5520
      Shape           =   5  'Rounded Square
      Top             =   1200
      Width           =   375
   End
   Begin VB.Shape Shape58 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   4800
      Shape           =   5  'Rounded Square
      Top             =   1200
      Width           =   375
   End
   Begin VB.Shape Shape57 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   4080
      Shape           =   5  'Rounded Square
      Top             =   1200
      Width           =   375
   End
   Begin VB.Shape Shape56 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   3360
      Shape           =   5  'Rounded Square
      Top             =   1200
      Width           =   375
   End
   Begin VB.Shape Shape55 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   2640
      Shape           =   5  'Rounded Square
      Top             =   1200
      Width           =   375
   End
   Begin VB.Shape Shape54 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   1920
      Shape           =   5  'Rounded Square
      Top             =   1200
      Width           =   375
   End
   Begin VB.Shape Shape53 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   1200
      Shape           =   5  'Rounded Square
      Top             =   1200
      Width           =   375
   End
   Begin VB.Shape Shape52 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   480
      Shape           =   5  'Rounded Square
      Top             =   1200
      Width           =   375
   End
   Begin VB.Shape Shape51 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   6960
      Shape           =   5  'Rounded Square
      Top             =   1920
      Width           =   375
   End
   Begin VB.Shape Shape50 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   6240
      Shape           =   5  'Rounded Square
      Top             =   1920
      Width           =   375
   End
   Begin VB.Shape Shape49 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   5520
      Shape           =   5  'Rounded Square
      Top             =   1920
      Width           =   375
   End
   Begin VB.Shape Shape48 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   4800
      Shape           =   5  'Rounded Square
      Top             =   1920
      Width           =   375
   End
   Begin VB.Shape Shape47 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   4080
      Shape           =   5  'Rounded Square
      Top             =   1920
      Width           =   375
   End
   Begin VB.Shape Shape46 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   3360
      Shape           =   5  'Rounded Square
      Top             =   1920
      Width           =   375
   End
   Begin VB.Shape Shape45 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   2640
      Shape           =   5  'Rounded Square
      Top             =   1920
      Width           =   375
   End
   Begin VB.Shape Shape44 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   1920
      Shape           =   5  'Rounded Square
      Top             =   1920
      Width           =   375
   End
   Begin VB.Shape Shape43 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   1200
      Shape           =   5  'Rounded Square
      Top             =   1920
      Width           =   375
   End
   Begin VB.Shape Shape42 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   480
      Shape           =   5  'Rounded Square
      Top             =   1920
      Width           =   375
   End
   Begin VB.Shape Shape41 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   6960
      Shape           =   5  'Rounded Square
      Top             =   2640
      Width           =   375
   End
   Begin VB.Shape Shape40 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   6240
      Shape           =   5  'Rounded Square
      Top             =   2640
      Width           =   375
   End
   Begin VB.Shape Shape39 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   5520
      Shape           =   5  'Rounded Square
      Top             =   2640
      Width           =   375
   End
   Begin VB.Shape Shape38 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   4800
      Shape           =   5  'Rounded Square
      Top             =   2640
      Width           =   375
   End
   Begin VB.Shape Shape37 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   4080
      Shape           =   5  'Rounded Square
      Top             =   2640
      Width           =   375
   End
   Begin VB.Shape Shape36 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   3360
      Shape           =   5  'Rounded Square
      Top             =   2640
      Width           =   375
   End
   Begin VB.Shape Shape35 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   2640
      Shape           =   5  'Rounded Square
      Top             =   2640
      Width           =   375
   End
   Begin VB.Shape Shape34 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   1920
      Shape           =   5  'Rounded Square
      Top             =   2640
      Width           =   375
   End
   Begin VB.Shape Shape33 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   1200
      Shape           =   5  'Rounded Square
      Top             =   2640
      Width           =   375
   End
   Begin VB.Shape Shape32 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   480
      Shape           =   5  'Rounded Square
      Top             =   2640
      Width           =   375
   End
   Begin VB.Shape Shape31 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   6960
      Shape           =   5  'Rounded Square
      Top             =   3360
      Width           =   375
   End
   Begin VB.Shape Shape30 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   6240
      Shape           =   5  'Rounded Square
      Top             =   3360
      Width           =   375
   End
   Begin VB.Shape Shape29 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   5520
      Shape           =   5  'Rounded Square
      Top             =   3360
      Width           =   375
   End
   Begin VB.Shape Shape28 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   4800
      Shape           =   5  'Rounded Square
      Top             =   3360
      Width           =   375
   End
   Begin VB.Shape Shape27 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   4080
      Shape           =   5  'Rounded Square
      Top             =   3360
      Width           =   375
   End
   Begin VB.Shape Shape26 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   3360
      Shape           =   5  'Rounded Square
      Top             =   3360
      Width           =   375
   End
   Begin VB.Shape Shape25 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   2640
      Shape           =   5  'Rounded Square
      Top             =   3360
      Width           =   375
   End
   Begin VB.Shape Shape24 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   1920
      Shape           =   5  'Rounded Square
      Top             =   3360
      Width           =   375
   End
   Begin VB.Shape Shape23 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   1200
      Shape           =   5  'Rounded Square
      Top             =   3360
      Width           =   375
   End
   Begin VB.Shape Shape22 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   480
      Shape           =   5  'Rounded Square
      Top             =   3360
      Width           =   375
   End
   Begin VB.Shape Shape21 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   6960
      Shape           =   5  'Rounded Square
      Top             =   4080
      Width           =   375
   End
   Begin VB.Shape Shape20 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   6240
      Shape           =   5  'Rounded Square
      Top             =   4080
      Width           =   375
   End
   Begin VB.Shape Shape19 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   5520
      Shape           =   5  'Rounded Square
      Top             =   4080
      Width           =   375
   End
   Begin VB.Shape Shape18 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   4800
      Shape           =   5  'Rounded Square
      Top             =   4080
      Width           =   375
   End
   Begin VB.Shape Shape17 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   4080
      Shape           =   5  'Rounded Square
      Top             =   4080
      Width           =   375
   End
   Begin VB.Shape Shape16 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   3360
      Shape           =   5  'Rounded Square
      Top             =   4080
      Width           =   375
   End
   Begin VB.Shape Shape15 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   2640
      Shape           =   5  'Rounded Square
      Top             =   4080
      Width           =   375
   End
   Begin VB.Shape Shape14 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   1920
      Shape           =   5  'Rounded Square
      Top             =   4080
      Width           =   375
   End
   Begin VB.Shape Shape13 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   1200
      Shape           =   5  'Rounded Square
      Top             =   4080
      Width           =   375
   End
   Begin VB.Shape Shape12 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   480
      Shape           =   5  'Rounded Square
      Top             =   4080
      Width           =   375
   End
   Begin VB.Shape Shape11 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   6960
      Shape           =   5  'Rounded Square
      Top             =   4800
      Width           =   375
   End
   Begin VB.Shape Shape10 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   6240
      Shape           =   5  'Rounded Square
      Top             =   4800
      Width           =   375
   End
   Begin VB.Shape Shape9 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   5520
      Shape           =   5  'Rounded Square
      Top             =   4800
      Width           =   375
   End
   Begin VB.Shape Shape8 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   4800
      Shape           =   5  'Rounded Square
      Top             =   4800
      Width           =   375
   End
   Begin VB.Shape Shape7 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   4080
      Shape           =   5  'Rounded Square
      Top             =   4800
      Width           =   375
   End
   Begin VB.Shape Shape6 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   3360
      Shape           =   5  'Rounded Square
      Top             =   4800
      Width           =   375
   End
   Begin VB.Shape Shape5 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   2640
      Shape           =   5  'Rounded Square
      Top             =   4800
      Width           =   375
   End
   Begin VB.Shape Shape4 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   1920
      Shape           =   5  'Rounded Square
      Top             =   4800
      Width           =   375
   End
   Begin VB.Shape Shape3 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   1200
      Shape           =   5  'Rounded Square
      Top             =   4800
      Width           =   375
   End
   Begin VB.Shape Shape2 
      FillColor       =   &H80000005&
      FillStyle       =   0  'Solid
      Height          =   375
      Left            =   480
      Shape           =   5  'Rounded Square
      Top             =   4800
      Width           =   375
   End
   Begin VB.Shape Shape1 
      FillColor       =   &H00FFC0FF&
      FillStyle       =   0  'Solid
      Height          =   7575
      Left            =   120
      Top             =   120
      Width           =   7575
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Command1_Click()

    Rem ----------------------------------------------- Riposizione ed avvio
    If Command1.Caption = "Ferma Simulazione" Then
       Image1.Top = 7200
       Image1.Left = 0
       Timer1.Enabled = False
       Command1.Caption = "Avvia Simulazione"
    Else
       Timer1.Enabled = True
       Command1.Caption = "Ferma Simulazione"
    End If
   
End Sub

Private Sub Form_Load()
   
    Rem ----------------------------------------------- Impostazioni Timer
    Timer1.Interval = 150
    Timer1.Enabled = True
    
    Rem ----------------------------------------------- Impostazioni Timer
    Command1.Caption = "Ferma Simulazione"

End Sub

Private Sub Timer1_Timer()
    
    Math.Randomize
    
    Rem ----------------------------------------------- Calcolo degli spostamenti
    Spostamento = Fix(Rnd() * 7 + 1)
    
    Rem ----------------------------------------------- Correzioni Errori, spostamenti, Max - Min
    If Image1.Left < 0 Then
       Image1.Left = 0
    End If
    If Image1.Left > 7200 Then
       Image1.Left = 7200
    End If
    If Image1.Top < 0 Then
       Image1.Top = 0
    End If
    If Image1.Top > 7200 Then
       Image1.Top = 7200
    End If
    
    Dim FeedBack As Integer
    FeedBack = FBack.Text
    
    If (Spostamento = FeedBack) Or (Spostamento = (FeedBack + 1)) Then
    Else
       If (Spostamento = 1) Or (Spostamento = 2) Then
       Rem ----------------------------------------------- DESTRO
          If Image1.Left = 7200 Then
             Image1.Left = Image1.Left - 720
          Else
             Image1.Left = Image1.Left + 720
             FBack.Text = 5
          End If
       ElseIf (Spostamento = 7) Or (Spostamento = 8) Then
       Rem ----------------------------------------------- SOTTO
          If Image1.Top = 7200 Then
             Image1.Top = Image1.Top - 720
          Else
             Image1.Top = Image1.Top + 720
             FBack.Text = 3
          End If
       ElseIf (Spostamento = 5) Or (Spostamento = 6) Then
       Rem ----------------------------------------------- SINISTRA
          If Image1.Left = 0 Then
             Image1.Left = Image1.Left + 720
          Else
             Image1.Left = Image1.Left - 720
             FBack.Text = 1
          End If
       ElseIf (Spostamento = 3) Or (Spostamento = 4) Then
       Rem ----------------------------------------------- SOPRA
          If Image1.Top = 0 Then
             Image1.Top = Image1.Top + 720
          Else
             Image1.Top = Image1.Top - 720
             FBack.Text = 7
          End If
       End If
    End If
    
    If (Spostamento = 1) Or (Spostamento = 2) Then
    Rem ----------------------------------------------- DESTRO
        Direzione.Text = "Destra"
    ElseIf (Spostamento = 7) Or (Spostamento = 8) Then
    Rem ----------------------------------------------- SOTTO
        Direzione.Text = "Sotto"
    ElseIf (Spostamento = 5) Or (Spostamento = 6) Then
    Rem ----------------------------------------------- SINISTRA
        Direzione.Text = "Sinistra"
    ElseIf (Spostamento = 3) Or (Spostamento = 4) Then
    Rem ----------------------------------------------- SOPRA
        Direzione.Text = "Sopra"
    End If
    
    If (Image1.Left = 6480) And (Image1.Top = 720) Then
    
        tmp = MsgBox("Formaggio raggiunto!!" & vbCrLf & "Il topo ha raggiunto la sua meta. Vuoi ricominciare?", vbYesNo)
        If tmp = vbYes Then
            Image1.Top = 7200
            Image1.Left = 0
            Timer1.Enabled = True
        Else
            Image1.Top = 7200
            Image1.Left = 0
            Timer1.Enabled = False
            Command1.Caption = "Avvia Simulazione"
        End If
        
    End If
   

End Sub
