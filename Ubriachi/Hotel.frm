VERSION 5.00
Begin VB.Form Form1 
   BorderStyle     =   1  'Fixed Single
   Caption         =   " Hotel Simulator"
   ClientHeight    =   5730
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   13440
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5730
   ScaleWidth      =   13440
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer Timer1 
      Left            =   0
      Top             =   5400
   End
   Begin VB.Frame Frame2 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   17
      Top             =   2640
      Width           =   10455
   End
   Begin VB.Frame Statistiche 
      Caption         =   "Statistiche"
      Height          =   5175
      Left            =   10920
      TabIndex        =   16
      Top             =   240
      Width           =   2415
      Begin VB.CommandButton Command1 
         Caption         =   "Ricomincia"
         Enabled         =   0   'False
         Height          =   255
         Left            =   240
         TabIndex        =   54
         Top             =   4800
         Width           =   1935
      End
      Begin VB.TextBox SVisitate 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   53
         Text            =   "0"
         Top             =   1320
         Width           =   2175
      End
      Begin VB.TextBox UCorrente 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   35
         Text            =   "1"
         Top             =   600
         Width           =   2175
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Stanze Visitate "
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   52
         Top             =   1080
         Width           =   2175
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Ubriaco Corrente "
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   34
         Top             =   360
         Width           =   2175
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Frame1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Index           =   15
      Left            =   9360
      TabIndex        =   15
      Top             =   3720
      Width           =   1215
      Begin VB.Label Visitate 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "..."
         Height          =   255
         Index           =   15
         Left            =   120
         TabIndex        =   51
         Top             =   1200
         Width           =   975
      End
      Begin VB.Label NomePersona 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "..."
         Height          =   255
         Index           =   15
         Left            =   120
         TabIndex        =   33
         Top             =   360
         Width           =   975
      End
      Begin VB.Image Occupato 
         Height          =   495
         Index           =   15
         Left            =   360
         Picture         =   "Hotel.frx":0000
         Stretch         =   -1  'True
         Top             =   600
         Width           =   495
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Frame1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Index           =   14
      Left            =   8040
      TabIndex        =   14
      Top             =   3720
      Width           =   1215
      Begin VB.Label Visitate 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "..."
         Height          =   255
         Index           =   14
         Left            =   120
         TabIndex        =   50
         Top             =   1200
         Width           =   975
      End
      Begin VB.Label NomePersona 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "..."
         Height          =   255
         Index           =   14
         Left            =   120
         TabIndex        =   32
         Top             =   360
         Width           =   975
      End
      Begin VB.Image Occupato 
         Height          =   495
         Index           =   14
         Left            =   360
         Picture         =   "Hotel.frx":2D6D2
         Stretch         =   -1  'True
         Top             =   600
         Width           =   495
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Frame1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Index           =   13
      Left            =   6720
      TabIndex        =   13
      Top             =   3720
      Width           =   1215
      Begin VB.Label Visitate 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "..."
         Height          =   255
         Index           =   13
         Left            =   120
         TabIndex        =   49
         Top             =   1200
         Width           =   975
      End
      Begin VB.Label NomePersona 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "..."
         Height          =   255
         Index           =   13
         Left            =   120
         TabIndex        =   31
         Top             =   360
         Width           =   975
      End
      Begin VB.Image Occupato 
         Height          =   495
         Index           =   13
         Left            =   360
         Picture         =   "Hotel.frx":5ADA4
         Stretch         =   -1  'True
         Top             =   600
         Width           =   495
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Frame1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Index           =   12
      Left            =   5400
      TabIndex        =   12
      Top             =   3720
      Width           =   1215
      Begin VB.Label Visitate 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "..."
         Height          =   255
         Index           =   12
         Left            =   120
         TabIndex        =   48
         Top             =   1200
         Width           =   975
      End
      Begin VB.Label NomePersona 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "..."
         Height          =   255
         Index           =   12
         Left            =   120
         TabIndex        =   30
         Top             =   360
         Width           =   975
      End
      Begin VB.Image Occupato 
         Height          =   495
         Index           =   12
         Left            =   360
         Picture         =   "Hotel.frx":88476
         Stretch         =   -1  'True
         Top             =   600
         Width           =   495
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Frame1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Index           =   11
      Left            =   4080
      TabIndex        =   11
      Top             =   3720
      Width           =   1215
      Begin VB.Label Visitate 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "..."
         Height          =   255
         Index           =   11
         Left            =   120
         TabIndex        =   47
         Top             =   1200
         Width           =   975
      End
      Begin VB.Label NomePersona 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "..."
         Height          =   255
         Index           =   11
         Left            =   120
         TabIndex        =   29
         Top             =   360
         Width           =   975
      End
      Begin VB.Image Occupato 
         Height          =   495
         Index           =   11
         Left            =   360
         Picture         =   "Hotel.frx":B5B48
         Stretch         =   -1  'True
         Top             =   600
         Width           =   495
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Frame1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Index           =   10
      Left            =   2760
      TabIndex        =   10
      Top             =   3720
      Width           =   1215
      Begin VB.Label Visitate 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "..."
         Height          =   255
         Index           =   10
         Left            =   120
         TabIndex        =   46
         Top             =   1200
         Width           =   975
      End
      Begin VB.Label NomePersona 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "..."
         Height          =   255
         Index           =   10
         Left            =   120
         TabIndex        =   28
         Top             =   360
         Width           =   975
      End
      Begin VB.Image Occupato 
         Height          =   495
         Index           =   10
         Left            =   360
         Picture         =   "Hotel.frx":E321A
         Stretch         =   -1  'True
         Top             =   600
         Width           =   495
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Frame1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Index           =   9
      Left            =   1440
      TabIndex        =   9
      Top             =   3720
      Width           =   1215
      Begin VB.Label Visitate 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "..."
         Height          =   255
         Index           =   9
         Left            =   120
         TabIndex        =   45
         Top             =   1200
         Width           =   975
      End
      Begin VB.Label NomePersona 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "..."
         Height          =   255
         Index           =   9
         Left            =   120
         TabIndex        =   27
         Top             =   360
         Width           =   975
      End
      Begin VB.Image Occupato 
         Height          =   495
         Index           =   9
         Left            =   360
         Picture         =   "Hotel.frx":1108EC
         Stretch         =   -1  'True
         Top             =   600
         Width           =   495
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Frame1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Index           =   8
      Left            =   120
      TabIndex        =   8
      Top             =   3720
      Width           =   1215
      Begin VB.Label Visitate 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "..."
         Height          =   255
         Index           =   8
         Left            =   120
         TabIndex        =   44
         Top             =   1200
         Width           =   975
      End
      Begin VB.Label NomePersona 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "..."
         Height          =   255
         Index           =   8
         Left            =   120
         TabIndex        =   26
         Top             =   360
         Width           =   975
      End
      Begin VB.Image Occupato 
         Height          =   495
         Index           =   8
         Left            =   360
         Picture         =   "Hotel.frx":13DFBE
         Stretch         =   -1  'True
         Top             =   600
         Width           =   495
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Frame1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Index           =   7
      Left            =   9360
      TabIndex        =   7
      Top             =   240
      Width           =   1215
      Begin VB.Label Visitate 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "..."
         Height          =   255
         Index           =   7
         Left            =   120
         TabIndex        =   43
         Top             =   1200
         Width           =   975
      End
      Begin VB.Label NomePersona 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "..."
         Height          =   255
         Index           =   7
         Left            =   120
         TabIndex        =   25
         Top             =   360
         Width           =   975
      End
      Begin VB.Image Occupato 
         Height          =   495
         Index           =   7
         Left            =   360
         Picture         =   "Hotel.frx":16B690
         Stretch         =   -1  'True
         Top             =   600
         Width           =   495
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Frame1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Index           =   6
      Left            =   8040
      TabIndex        =   6
      Top             =   240
      Width           =   1215
      Begin VB.Label Visitate 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "..."
         Height          =   255
         Index           =   6
         Left            =   120
         TabIndex        =   42
         Top             =   1200
         Width           =   975
      End
      Begin VB.Label NomePersona 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "..."
         Height          =   255
         Index           =   6
         Left            =   120
         TabIndex        =   24
         Top             =   360
         Width           =   975
      End
      Begin VB.Image Occupato 
         Height          =   495
         Index           =   6
         Left            =   360
         Picture         =   "Hotel.frx":198D62
         Stretch         =   -1  'True
         Top             =   600
         Width           =   495
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Frame1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Index           =   5
      Left            =   6720
      TabIndex        =   5
      Top             =   240
      Width           =   1215
      Begin VB.Label Visitate 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "..."
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   41
         Top             =   1200
         Width           =   975
      End
      Begin VB.Label NomePersona 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "..."
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   23
         Top             =   360
         Width           =   975
      End
      Begin VB.Image Occupato 
         Height          =   495
         Index           =   5
         Left            =   360
         Picture         =   "Hotel.frx":1C6434
         Stretch         =   -1  'True
         Top             =   600
         Width           =   495
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Frame1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Index           =   4
      Left            =   5400
      TabIndex        =   4
      Top             =   240
      Width           =   1215
      Begin VB.Label Visitate 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "..."
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   40
         Top             =   1200
         Width           =   975
      End
      Begin VB.Label NomePersona 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "..."
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   22
         Top             =   360
         Width           =   975
      End
      Begin VB.Image Occupato 
         Height          =   495
         Index           =   4
         Left            =   360
         Picture         =   "Hotel.frx":1F3B06
         Stretch         =   -1  'True
         Top             =   600
         Width           =   495
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Frame1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Index           =   3
      Left            =   4080
      TabIndex        =   3
      Top             =   240
      Width           =   1215
      Begin VB.Label Visitate 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "..."
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   39
         Top             =   1200
         Width           =   975
      End
      Begin VB.Label NomePersona 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "..."
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   21
         Top             =   360
         Width           =   975
      End
      Begin VB.Image Occupato 
         Height          =   495
         Index           =   3
         Left            =   360
         Picture         =   "Hotel.frx":2211D8
         Stretch         =   -1  'True
         Top             =   600
         Width           =   495
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Camera N�3"
      Height          =   1695
      Index           =   2
      Left            =   2760
      TabIndex        =   2
      Top             =   240
      Width           =   1215
      Begin VB.Label Visitate 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "..."
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   38
         Top             =   1200
         Width           =   975
      End
      Begin VB.Label NomePersona 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "..."
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   20
         Top             =   360
         Width           =   975
      End
      Begin VB.Image Occupato 
         Height          =   495
         Index           =   2
         Left            =   360
         Picture         =   "Hotel.frx":24E8AA
         Stretch         =   -1  'True
         Top             =   600
         Width           =   495
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Camera N�2"
      Height          =   1695
      Index           =   1
      Left            =   1440
      TabIndex        =   1
      Top             =   240
      Width           =   1215
      Begin VB.Label Visitate 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "..."
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   37
         Top             =   1200
         Width           =   975
      End
      Begin VB.Label NomePersona 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "..."
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   19
         Top             =   360
         Width           =   975
      End
      Begin VB.Image Occupato 
         Height          =   495
         Index           =   1
         Left            =   360
         Picture         =   "Hotel.frx":27BF7C
         Stretch         =   -1  'True
         Top             =   600
         Width           =   495
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Camera N�1"
      Height          =   1695
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   1215
      Begin VB.Image Occupato 
         Height          =   495
         Index           =   0
         Left            =   360
         Picture         =   "Hotel.frx":2A964E
         Stretch         =   -1  'True
         Top             =   600
         Width           =   495
      End
      Begin VB.Label Visitate 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "..."
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   36
         Top             =   1200
         Width           =   975
      End
      Begin VB.Label NomePersona 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "..."
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   18
         Top             =   360
         Width           =   975
      End
   End
   Begin VB.Image Esterni 
      Height          =   495
      Index           =   15
      Left            =   9720
      Picture         =   "Hotel.frx":2D6D20
      Stretch         =   -1  'True
      Top             =   3120
      Width           =   495
   End
   Begin VB.Image Esterni 
      Height          =   495
      Index           =   14
      Left            =   8400
      Picture         =   "Hotel.frx":3043F2
      Stretch         =   -1  'True
      Top             =   3120
      Width           =   495
   End
   Begin VB.Image Esterni 
      Height          =   495
      Index           =   13
      Left            =   7080
      Picture         =   "Hotel.frx":331AC4
      Stretch         =   -1  'True
      Top             =   3120
      Width           =   495
   End
   Begin VB.Image Esterni 
      Height          =   495
      Index           =   12
      Left            =   5760
      Picture         =   "Hotel.frx":35F196
      Stretch         =   -1  'True
      Top             =   3120
      Width           =   495
   End
   Begin VB.Image Esterni 
      Height          =   495
      Index           =   11
      Left            =   4440
      Picture         =   "Hotel.frx":38C868
      Stretch         =   -1  'True
      Top             =   3120
      Width           =   495
   End
   Begin VB.Image Esterni 
      Height          =   495
      Index           =   10
      Left            =   3120
      Picture         =   "Hotel.frx":3B9F3A
      Stretch         =   -1  'True
      Top             =   3120
      Width           =   495
   End
   Begin VB.Image Esterni 
      Height          =   495
      Index           =   9
      Left            =   1800
      Picture         =   "Hotel.frx":3E760C
      Stretch         =   -1  'True
      Top             =   3120
      Width           =   495
   End
   Begin VB.Image Esterni 
      Height          =   495
      Index           =   8
      Left            =   480
      Picture         =   "Hotel.frx":414CDE
      Stretch         =   -1  'True
      Top             =   3120
      Width           =   495
   End
   Begin VB.Image Esterni 
      Height          =   495
      Index           =   7
      Left            =   9720
      Picture         =   "Hotel.frx":4423B0
      Stretch         =   -1  'True
      Top             =   2040
      Width           =   495
   End
   Begin VB.Image Esterni 
      Height          =   495
      Index           =   6
      Left            =   8400
      Picture         =   "Hotel.frx":46FA82
      Stretch         =   -1  'True
      Top             =   2040
      Width           =   495
   End
   Begin VB.Image Esterni 
      Height          =   495
      Index           =   5
      Left            =   7080
      Picture         =   "Hotel.frx":49D154
      Stretch         =   -1  'True
      Top             =   2040
      Width           =   495
   End
   Begin VB.Image Esterni 
      Height          =   495
      Index           =   4
      Left            =   5760
      Picture         =   "Hotel.frx":4CA826
      Stretch         =   -1  'True
      Top             =   2040
      Width           =   495
   End
   Begin VB.Image Esterni 
      Height          =   495
      Index           =   3
      Left            =   4440
      Picture         =   "Hotel.frx":4F7EF8
      Stretch         =   -1  'True
      Top             =   2040
      Width           =   495
   End
   Begin VB.Image Esterni 
      Height          =   495
      Index           =   2
      Left            =   3120
      Picture         =   "Hotel.frx":5255CA
      Stretch         =   -1  'True
      Top             =   2040
      Width           =   495
   End
   Begin VB.Image Esterni 
      Height          =   495
      Index           =   1
      Left            =   1800
      Picture         =   "Hotel.frx":552C9C
      Stretch         =   -1  'True
      Top             =   2040
      Width           =   495
   End
   Begin VB.Image Esterni 
      Height          =   495
      Index           =   0
      Left            =   480
      Picture         =   "Hotel.frx":58036E
      Stretch         =   -1  'True
      Top             =   2040
      Width           =   465
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()

    Timer1.Enabled = True
    For n = 0 To 15 Step 1
        Occupato(n).Visible = False
        NomePersona(n).Caption = "..."
        Visitate(n).Caption = "..."
    Next

End Sub

Private Sub Form_Load()

    For Stat = 0 To 15 Step 1
        Frame1(Stat).FontName = "Arial"
        Frame1(Stat).Caption = "Camera N�" & (Stat + 1)
        Esterni(Stat).Visible = False
    Next
    
    For Stat1 = 0 To 15 Step 1
        Occupato(Stat1).Visible = False
    Next
    
    Timer1.Interval = 1000
    Timer1.Enabled = True

End Sub

Private Sub Timer1_Timer()
    
    Math.Randomize
    Command1.Enabled = False
    
    Rem nascondi le immagini tizio
    For i = 0 To 15 Step 1
        Esterni(i).Visible = False
    Next
    
    Rem Calcolo casuale della posizione del tizio
    UEsterni = Fix(Rnd() * 16)
    
    Rem spostamento tizio
    Esterni(UEsterni).Visible = True
    
    Rem Controllo stanza
    If NomePersona(UEsterni).Caption = "..." Then
        Occupato(UEsterni).Visible = True
        NomePersona(UEsterni).Caption = "Utente " & UCorrente.Text
        Visitate(UEsterni).Caption = "Visitate " & SVisitate.Text
        UCorrente.Text = UCorrente.Text + 1
        SVisitate.Text = 0
    Else
        SVisitate.Text = SVisitate.Text + 1
    End If
    
    If UCorrente.Text = "17" Then
        Timer1.Enabled = False
        UCorrente.Text = 1
        For i = 0 To 15 Step 1
            Esterni(i).Visible = False
        Next
        SVisitate.Text = 0
        Command1.Enabled = True
    End If
    
End Sub

