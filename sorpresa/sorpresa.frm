VERSION 5.00
Begin VB.Form Form1 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   Caption         =   "Pacco "
   ClientHeight    =   4695
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   6375
   LinkTopic       =   "Form1"
   ScaleHeight     =   4695
   ScaleWidth      =   6375
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer Timer2 
      Enabled         =   0   'False
      Interval        =   10
      Left            =   480
      Top             =   4320
   End
   Begin VB.TextBox txt_time 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   56.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1185
      Left            =   0
      TabIndex        =   3
      Text            =   "10"
      Top             =   120
      Visible         =   0   'False
      Width           =   6375
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   0
      Top             =   4320
   End
   Begin VB.Frame obj_frame 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   1095
      Left            =   2160
      TabIndex        =   0
      Top             =   1440
      Width           =   2055
      Begin VB.CommandButton Command1 
         Caption         =   "Clicca Qui"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   1
         Top             =   550
         Width           =   1815
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         Caption         =   "Scopri Ora!!"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   2
         Top             =   220
         Width           =   1815
      End
   End
   Begin VB.Label lbl_random 
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      Height          =   255
      Left            =   5760
      TabIndex        =   4
      Top             =   4440
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.Image img_sor 
      Appearance      =   0  'Flat
      Height          =   2700
      Left            =   2160
      Picture         =   "sorpresa.frx":0000
      Top             =   1680
      Visible         =   0   'False
      Width           =   2325
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()

   If Command1.Caption = "Clicca Qui" Then
      obj_frame.Visible = False
      img_sor.Visible = True
      Timer1.Enabled = True
      txt_time.Visible = True
   Else
      img_sor.Visible = False
      Timer1.Enabled = False
      Timer2.Enabled = False
      txt_time.Visible = False
      txt_time.Text = "10"
      obj_frame.Visible = True
      Label1.Caption = "Scopri Ora!!"
      Command1.Caption = "Clicca Qui"
      Form1.Cls
   End If

End Sub

Private Sub Form_Resize()

   h = Form1.Height
   w = Form1.Width
   Form1.Cls
   
   Rem messaggio
   obj_frame.Left = (w / 2) - (2055 / 2)
   obj_frame.Top = (h / 2) - (1095 / 2) - 300
   
   Rem pacchetto
   img_sor.Left = (w / 2) - (2325 / 2)
   img_sor.Top = h - 3600
   
   Rem timer
   txt_time.Width = Form1.Width

End Sub

Private Sub Timer1_Timer()

   Form1.ScaleHeight = Form1.Height
   Form1.ScaleWidth = Form1.Width
   Form1.ScaleMode = 0
   Form1.ScaleTop = 0
   Form1.ScaleLeft = 0
   
   If txt_time.Text <> 0 Then
     txt_time.Text = txt_time.Text - 1
   Else
     img_sor.Visible = False
     Timer1.Enabled = False
     Timer2.Enabled = True
     txt_time.Visible = False
     obj_frame.Visible = True
     Label1.Caption = "Sorpresa!!"
     Command1.Caption = "Nuovo"
     If lbl_random.Caption = 0 Then
       lbl_random.Caption = 1
     ElseIf lbl_random.Caption = 2 Then
       lbl_random.Caption = 0
     Else
       lbl_random.Caption = 2
     End If
   End If

End Sub

Private Sub Timer2_Timer()

   If lbl_random.Caption = 0 Then
     Timer2.Interval = 10
     x = Rnd() * Form1.Width + 1
     y = Rnd() * Form1.Height + 1
     r = Rnd() * Form1.Width + 1
     Form1.DrawWidth = 1
     Form1.Circle (x, y), r
   ElseIf lbl_random.Caption = 1 Then
      Timer2.Interval = 100
      If txt_time.Text < 20 Then
        state_win = Rnd() * 1 + 1
        If Form1.WindowState <> state_win Then
          Form1.WindowState = state_win
        Else
          state_win = Rnd() * 0 + 1
          Form1.WindowState = state_win
        End If
        txt_time.Text = txt_time.Text + 1
      Else
        Timer2.Enabled = False
        Form1.WindowState = 2
      End If
   Else
     Timer2.Interval = 1
     x = Rnd() * Form1.Width + 1
     y = Rnd() * Form1.Height + 1
     Form1.DrawWidth = 3
     Form1.PSet (x, y)
   End If
   
End Sub
